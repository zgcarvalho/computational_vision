#%%
import itertools
from scipy import ndimage, spatial
import matplotlib.pyplot as plt
import numpy as np
from glob import glob
import os.path
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

def load_image(fn):
    return ndimage.imread(fn)

def fill(im):
    return ndimage.binary_fill_holes(im)

def erode(im_filled,n):
    new_im = ndimage.binary_erosion(im_filled,iterations=n)
    return float(ndimage.sum(new_im))/ndimage.sum(im_filled)

def load_data(path):
    files = glob(path+"/*.png")
    classes = sorted(list(set([os.path.basename(x)[:-9] for x in files])))
    print(classes)
    x = []
    y = []
    for i in files:
        y.append(classes.index(os.path.basename(i)[:-9]))
        x.append(get_features(i))
        # print(i)
        # print(x)
        # print(y)
    return np.asarray(x), np.asarray(y), classes

def get_features(fn):
    im = load_image(fn)
    im_contour = ndimage.binary_closing(im)
    im_filled = fill(im_contour)
    # feat = get_area(im_filled)
    # feat += get_perimeter(im_contour)
    feat = get_area_perimeter(im_contour, im_filled)
    feat += get_stats_contour(im_contour,im_filled)
    feat += get_erode(im_filled,2)
    feat += get_erode(im_filled,5)
    feat += get_erode(im_filled,10)
    feat += get_dilate(im_filled,2)
    feat += get_dilate(im_filled,5)
    feat += get_dilate(im_filled,10)
    return feat

def get_area_perimeter(im_contour, im_filled):
    return [ndimage.sum(im_filled)/(ndimage.sum(im_contour)**2)]

def get_area(im_filled):
    return [ndimage.sum(im_filled)]

def get_perimeter(im_contour):
    return [ndimage.sum(im_contour)]

def get_erode(im_filled,n):
    new_im = ndimage.binary_erosion(im_filled,iterations=n)
    return [float(ndimage.sum(new_im))/ndimage.sum(im_filled)]

def get_dilate(im_filled,n):
    new_im = ndimage.binary_dilation(im_filled,iterations=n)
    return [float(ndimage.sum(new_im))/ndimage.sum(im_filled)]

def get_cm(im_filled):
    return [ndimage.center_of_mass(im_filled)]

def get_stats_contour(im_contour,im_filled):
    x_cm, y_cm = ndimage.center_of_mass(im_filled)
    contour = np.where(im_contour==True)
    min_h, min_w = np.min(contour,axis=1)
    max_h, max_w = np.max(contour,axis=1)
    # print(max_h, max_w)
    #distance from cm to top
    dist_cm_top = abs(min_h - y_cm)
    # print(dist_cm_top)
    #distance from cm to bottom
    dist_cm_bottom = abs(max_h - y_cm)
    # print(dist_cm_bottom)
    #distance from cm to left
    dist_cm_left = abs(min_w - x_cm)
    # print(dist_cm_left)
    #distance from cm to right
    dist_cm_right = abs(max_w - x_cm)
    # print(dist_cm_right)
    points_contour = np.transpose([contour[1],contour[0]])
    dists = spatial.distance.cdist(np.asarray([[x_cm,y_cm]]), points_contour)
    mean = np.mean(dists)
    median = np.median(dists)/mean
    perc_1 = np.percentile(dists,1)/mean
    perc_5 = np.percentile(dists,5)/mean
    perc_10 = np.percentile(dists,10)/mean
    perc_25 = np.percentile(dists,25)/mean
    perc_75 = np.percentile(dists,75)/mean
    perc_90 = np.percentile(dists,90)/mean
    perc_95 = np.percentile(dists,95)/mean
    perc_99 = np.percentile(dists,99)/mean
    return [dist_cm_top/mean, dist_cm_bottom/mean, dist_cm_left/mean, dist_cm_right/mean, median,perc_1,perc_5, perc_10, perc_25, perc_75, perc_90, perc_95, perc_99]



def knn_train(x, y, n_neighbors):
    h=.2
    clf = neighbors.KNeighborsClassifier(n_neighbors, weights='distance')
    clf.fit(x,y)

    return clf


def accuracy(y, pred):
    t = 0
    f = 0
    for i in range(len(y)):
        if y[i]==pred[i]:
            t += 1
        else:
            f += 1
    return float(t)/len(y)


def plot_confusion_matrix(cm, classes):

    plt.figure(figsize=(20,10))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title('Confusion matrix')
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def main():
    x_train, y_train, classes = load_data("./proj02/train")
    x_test, y_test, classes = load_data("./proj02/test")
    # print(x_train, y_train)

    #PCA
    pca = PCA()
    pca.fit(x_train, y_train)
    print(pca.explained_variance_) 
    print(pca.explained_variance_ratio_) 

    #KMeans
    kmeans = KMeans(n_clusters=15).fit(x_train, y=y_train)
    print(kmeans.labels_)
    print(y_train)
    print(kmeans.predict(x_test))
    print(y_test)

    #KNN
    model = knn_train(x_train,y_train,3) 
    pred = model.predict(x_test)
    # print("Pred")
    # print(pred)
    # print(y_test)
    # print(accuracy(y_test,pred))

    print("k Nearest Neighbors")
    print("Accuracy:", accuracy_score(y_test, pred))
    cm = confusion_matrix(y_test, pred)
    plot_confusion_matrix(cm,classes=classes)
    # im = load_image("./proj02/Leaves256x256c/limao_01_a.png")
    # im_contour = ndimage.binary_closing(im)
    # im_filled = fill(im_contour)
    # plt.imshow(im_contour)
    # plt.show()

    
    
    # #area
    # print(ndimage.sum(im_filled))
    # #perimeter
    # print(ndimage.sum(im_contour))
    # #center of mass
    # x_cm, y_cm = ndimage.center_of_mass(im_filled)
    # print(x_cm, y_cm)
    # #points of the contour
    # contour = np.where(im_contour==True)
    # min_h, min_w = np.min(contour,axis=1)
    # max_h, max_w = np.max(contour,axis=1)
    # # print(max_h, max_w)
    # #distance from cm to top
    # dist_cm_top = abs(min_h - y_cm)
    # print(dist_cm_top)
    # #distance from cm to bottom
    # dist_cm_bottom = abs(max_h - y_cm)
    # print(dist_cm_bottom)
    # #distance from cm to left
    # dist_cm_left = abs(min_w - x_cm)
    # print(dist_cm_left)
    # #distance from cm to right
    # dist_cm_right = abs(max_w - x_cm)
    # print(dist_cm_right)
    # #distances from cm to contour
    # points_contour = np.transpose([contour[1],contour[0]])
    # dists = spatial.distance.cdist(np.asarray([[x_cm,y_cm]]), points_contour)
    # print(np.median(dists))
    # print(np.mean(dists))
    # print(np.percentile(dists,10))
    # print(np.percentile(dists,25))
    # print(np.percentile(dists,75))
    # print(np.percentile(dists,90))
    # print(ndimage.maximum_position(im_contour,labels=im_contour))
    # print(ndimage.center_of_mass(im_contour)-ndimage.center_of_mass(im_filled))

    # im2 = apply_gauss(im,2)
    # plt.imshow(im2)
    # plt.show()

    # im3 = apply_log(im,2)
    # plt.imshow(im3)
    # plt.show()




if __name__ == '__main__':
    main()
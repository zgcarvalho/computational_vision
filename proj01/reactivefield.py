#%%

from scipy import ndimage, fftpack, signal
import matplotlib.pyplot as plt
import numpy as np


def load_image(fn):
    return ndimage.imread(fn, mode='L')

def fft2(im):
    return fftpack.fft2(im)

def ifft2(imfft):
    return fftpack.ifft2(imfft)

def gauss(height, width, sigma):
    h = signal.gaussian(height,sigma)
    w = signal.gaussian(width,sigma)
    return np.dot(np.transpose([h]),[w])

def log(height, width, sigma):
    return ndimage.filters.laplace(gauss(height,width,sigma))

def apply_gauss(im, sigma):
    imfft = fft2(im)
    h,w = imfft.shape
    filter = fft2(gauss(h,w,sigma))
    y = np.multiply(imfft,filter)
    im = ifft2(y)
    # im.astype('B')
    tmp = abs(fftpack.fftshift(im))
    result = np.digitize(tmp,np.arange(np.min(tmp),np.max(tmp),(np.max(tmp)-np.min(tmp))/256.0 ))-1
    return result
    

def apply_log(im, sigma):
    imfft = fft2(im)
    h,w = imfft.shape
    filter = fft2(log(h,w,sigma))
    y = np.multiply(imfft,filter)
    im = ifft2(y)
    tmp = abs(fftpack.fftshift(im))
    result = np.digitize(tmp,np.arange(np.min(tmp),np.max(tmp),(np.max(tmp)-np.min(tmp))/256.0 ))-1
    return result


def main():
    im = load_image("./proj01/banksy.png")
    plt.imshow(im)
    plt.show()

    im2 = apply_gauss(im,2)
    plt.imshow(im2)
    plt.show()

    im3 = apply_log(im,2)
    plt.imshow(im3)
    plt.show()




if __name__ == '__main__':
    main()
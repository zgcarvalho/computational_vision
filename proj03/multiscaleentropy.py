#%%
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as sc
sys.path.append("./proj01")
from reactivefield import load_image, apply_gauss

def entropy(im):
    unique, counts = np.unique(im, return_counts=True)
    # print(dict(zip(unique, counts)))
    return sc.entropy(counts, base=256)

def multiscale_entropy(im, sigma_range):
    ent = [entropy(im)]
    for s in sigma_range:
        ent.append(entropy(apply_gauss(im,s)))
    return ent

def main():
    IMAGE_FILE = "./proj03/Original_Brodatz/D42.gif"
    SIGMA_MAX = 5.0
    SIGMA_STEP = 0.1

    im = load_image(IMAGE_FILE)
    plt.imshow(im)
    plt.show()

    im2 = apply_gauss(im, SIGMA_MAX)
    plt.imshow(im2)
    plt.show()

    plt.plot(np.arange(0.0,SIGMA_MAX,SIGMA_STEP),multiscale_entropy(im,np.arange(SIGMA_STEP, SIGMA_MAX, SIGMA_STEP)))
    plt.show()


if __name__ == '__main__':
    main()

